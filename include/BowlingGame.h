/*
 * BowlingGame.h
 *
 *  Created on: 2019�~12��14��
 *      Author: Happiness
 */

#ifndef BOWLINGGAME_H_
#define BOWLINGGAME_H_

void BowlingGame_init();
void BowlingGame_roll(int pins);
int BowlingGame_score();

#endif /* BOWLINGGAME_H_ */
