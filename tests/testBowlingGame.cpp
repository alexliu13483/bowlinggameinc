/*
 * testBowlingGame.cpp
 *
 *  Created on: 2019�~12��14��
 *      Author: Happiness
 */

/* Useful CppUTest Assert Functions
 * 	STRCMP_EQUAL(expect, result);
 * 	LONGS_EQUAL(expect, result);
 * 	BYTES_EQUAL(expect, result);
 * 	POINTERS_EQUAL(expect, result);
 *  DOUBLES_EQUAL(expected,actual,threshold);
 */



extern "C"
{
#include "BowlingGame.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(testBowlingGame)
{

    void setup()
    {
    	BowlingGame_init();
    }

    void teardown()
    {

    }

    void repeatRolls(int pins, int times) {
    	for (int i = 0;  i < times; i++)
    		BowlingGame_roll(pins);
    }
};

TEST(testBowlingGame, mixSpareStrikes)
{
	BowlingGame_roll(10);
	BowlingGame_roll(0);
	BowlingGame_roll(6);
	BowlingGame_roll(3);
	BowlingGame_roll(7);
	BowlingGame_roll(3);
	BowlingGame_roll(5);
	repeatRolls(0, 12);
	LONGS_EQUAL(43, BowlingGame_score());
}

TEST(testBowlingGame, allStrikes)
{
	repeatRolls(10, 12);
	LONGS_EQUAL(300, BowlingGame_score());
}

TEST(testBowlingGame, oneStrike)
{
	BowlingGame_roll(10);
	BowlingGame_roll(3);
	BowlingGame_roll(5);
	repeatRolls(0, 16);
	LONGS_EQUAL(26, BowlingGame_score());
}

TEST(testBowlingGame, oneSpare)
{
	BowlingGame_roll(4);
	BowlingGame_roll(6);
	BowlingGame_roll(5);
	repeatRolls(0, 17);
	LONGS_EQUAL(20, BowlingGame_score());
}

TEST(testBowlingGame, scoreOne)
{
	BowlingGame_roll(1);
	repeatRolls(0, 19);

	LONGS_EQUAL(1, BowlingGame_score());
}

TEST(testBowlingGame, guttering)
{
	repeatRolls(0, 20);
	LONGS_EQUAL(0, BowlingGame_score());
}
