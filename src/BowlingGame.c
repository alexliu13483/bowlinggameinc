/*
 * BowlingGame.c
 *
 *  Created on: 2019�~12��14��
 *      Author: Happiness
 */

#include <stdbool.h>

#define MAX_ROLLS	21
static int rolls[MAX_ROLLS];
static int currentRoll;

void BowlingGame_init() {
	currentRoll = 0;
	for (int i = 0; i < MAX_ROLLS; i++)
		rolls[i] = 0;
}

void BowlingGame_roll(int pins) {
	rolls[currentRoll++] = pins;
}

static bool isStrike(int i) {
	return rolls[i] == 10;
}

static bool isSpare(int i) {
	return rolls[i] + rolls[i + 1] == 10;
}

static int calStrikeScore(int i) {
	return 10 + rolls[i + 1] + rolls[i + 2];
}

static int calSpareScore(int i) {
	return 10 + rolls[i + 2];
}

static int calNormalScore(int i) {
	return rolls[i] + rolls[i + 1];
}

int BowlingGame_score() {
	int score = 0;
	int i = 0;

	for (int frame = 0; frame < 10; frame++) {
		if (isStrike(i)) { // Strike
			score += calStrikeScore(i);
			i++;
		} else if (isSpare(i)) { // spare
			score += calSpareScore(i);
			i += 2;
		}
		else {
			score += calNormalScore(i);
			i += 2;
		}
	}
	return score;
}
